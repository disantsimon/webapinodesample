import { Controller, HttpGET } from "webapimodel";

@Controller()
export class Product {

    @HttpGET()
    get(request, response) {
        response.send("product " + (response.locals.server ? "on port " + response.locals.server : ""));
    }
}