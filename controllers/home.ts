import { HttpGET, Controller, HttpPOST } from 'webapimodel'

@Controller()
export class HomeController {

    @HttpGET()
    get(request, response, next) {
        // get request home/ 
        response.send("this is home controller " + (response.locals.server ? "on port " + response.locals.server : ""));
    }

    @HttpPOST()
    post(request, response, next) {
        // post request home/ 
        response.json({ msg: "your data has been posted" })
    }

    @HttpPOST("send")
    sendMessage(request, response, next) {
        //http request post to home/send
        response.json({ msg: "your message has been posted" })
    }
}
