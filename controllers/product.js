"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var webapimodel_1 = require("webapimodel");
var Product = /** @class */ (function () {
    function Product() {
    }
    Product.prototype.get = function (request, response) {
        response.send("product " + (response.locals.server ? "on port " + response.locals.server : ""));
    };
    __decorate([
        webapimodel_1.HttpGET()
    ], Product.prototype, "get");
    Product = __decorate([
        webapimodel_1.Controller()
    ], Product);
    return Product;
}());
exports.Product = Product;
