import { Controller, HttpGET, HttpPOST } from "webapimodel";

@Controller("us")
export class User {

    @HttpGET()
    get(request, response) {
        // get request us/
        response.send("user " + (response.locals.server ? "on port " + response.locals.server : ""));
    }

    @HttpPOST()
    post(request, response, next) {
        // post request us/
    }

    @HttpPOST("list")
    getAllUser(request, response, next) {
        // post request us/list
    }
}