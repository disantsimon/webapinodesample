"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var webapimodel_1 = require("webapimodel");
var User = /** @class */ (function () {
    function User() {
    }
    User.prototype.get = function (request, response) {
        // get request us/
        response.send("user " + (response.locals.server ? "on port " + response.locals.server : ""));
    };
    User.prototype.post = function (request, response, next) {
        // post request us/
    };
    User.prototype.getAllUser = function (request, response, next) {
        // post request us/list
    };
    __decorate([
        webapimodel_1.HttpGET()
    ], User.prototype, "get");
    __decorate([
        webapimodel_1.HttpPOST()
    ], User.prototype, "post");
    __decorate([
        webapimodel_1.HttpPOST("list")
    ], User.prototype, "getAllUser");
    User = __decorate([
        webapimodel_1.Controller("us")
    ], User);
    return User;
}());
exports.User = User;
