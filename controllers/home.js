"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var webapimodel_1 = require("webapimodel");
var HomeController = /** @class */ (function () {
    function HomeController() {
    }
    HomeController.prototype.get = function (request, response, next) {
        // get request home/ 
        response.send("this is home controller " + (response.locals.server ? "on port " + response.locals.server : ""));
    };
    HomeController.prototype.post = function (request, response, next) {
        // post request home/ 
        response.json({ msg: "your data has been posted" });
    };
    HomeController.prototype.sendMessage = function (request, response, next) {
        //http request post to home/send
        response.json({ msg: "your message has been posted" });
    };
    __decorate([
        webapimodel_1.HttpGET()
    ], HomeController.prototype, "get");
    __decorate([
        webapimodel_1.HttpPOST()
    ], HomeController.prototype, "post");
    __decorate([
        webapimodel_1.HttpPOST("send")
    ], HomeController.prototype, "sendMessage");
    HomeController = __decorate([
        webapimodel_1.Controller()
    ], HomeController);
    return HomeController;
}());
exports.HomeController = HomeController;
