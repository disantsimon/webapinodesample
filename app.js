"use strict";
exports.__esModule = true;
var webapimodel_1 = require("webapimodel");
var product_1 = require("./controllers/product");
require("./Controllers");
var srv1 = new webapimodel_1.WebApiServer().listen(3020);
var srv2 = new webapimodel_1.WebApiServer().listen(3021);
srv1.start();
srv2.getExpressInstance().use(function (request, response, next) {
    response.locals.server = 3021;
    next();
});
srv2.start([product_1.Product]);
