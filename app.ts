import { WebApiServer } from 'webapimodel'
import { Product } from './controllers/product';

declare var require;

require("./Controllers")


/**
 * create a webApiServer and start listen on port
 */
let srv1 = new WebApiServer().listen(3020);

let srv2 = new WebApiServer().listen(3021);

/**
 * each class controller 
 */
srv1.start();

/**
 * get the express instance created
 */
var appSrv2 = srv1.getExpressInstance();
appSrv2.listen(3099, (err: string) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${3099}`)
})

/**
 * give a specific controller instance
 */
var controllerProduct = srv1.getController<Product>(Product);

srv2.getExpressInstance().use(function (request, response, next) {
    response.locals.server = 3021;
    next()
})

/**
 * only given controller will start
 */
srv2.start([Product]);

